class AddSlugToTags < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :slug, :string
    Rake::Task["db:generate_slugs"].invoke
    change_column :tags, :slug, :string, :null => false
  end
end
