class AddIndexToMemesTagsJoinTable < ActiveRecord::Migration[5.0]
  def change
    add_index :memes_tags, [ :meme_id, :tag_id ], :unique => true
  end
end
