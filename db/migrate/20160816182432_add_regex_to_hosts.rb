class AddRegexToHosts < ActiveRecord::Migration[5.0]
  def change
    add_column :hosts, :regex, :string
  end
end
