class CreateHosts < ActiveRecord::Migration[5.0]
  def change
    create_table :hosts do |t|
      t.string :name
      t.string :url
      t.boolean :active
      t.attachment :broken_img
    end

    add_index :hosts, :url, :unique => true
    add_reference :memes, :host, :foreign_key => true

    remove_column :memes, :url, :string
    add_column :memes, :filename, :string
  end
end
