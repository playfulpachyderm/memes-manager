# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts "Generating users..."
User.create!(:name => "Alessio", :email => "playful.pachyderm@gmail.com",
  :password =>        "Jostaphius the Sun God",
  :password_confirmation =>  "Jostaphius the Sun God",
  :admin => true, :activated => true, :activated_at => Time.zone.now)
puts "User: Alessio"

User.create!(:name => "asdf", :email => "asdf@asdf.com",
  :password => "asdfasdf", :password_confirmation => "asdfasdf",
  :activated => true, :activated_at => Time.zone.now)
puts "User: asdf"
puts

puts "Generating tags..."
["best hashtag", "dangerous donald", "hitlery", "rare donalds", "tweets",
  "jeb is a mess", "stumped", "bernard", "current year", "god emperor",
  "racist", "the picture", "wall", "constantinople", "gifs", "smuggies",
  "ivanka", "murica", "ashamed eating your face thing", "Sad!",
  "think regressive", "milo", "maga", "high energy", "trump face",
  "eurabia", "rubot", "trump quotes", "pepe", "President Trump",
  "before and after", "bldmtblm", "daily struggle", "liberal tears",
  "citation needed", "farage", "pocahontas", "pence", "comparison",
  "unstumpable", "nimble"].each do |tag|

  tag = Tag.new(:name => tag)
  tag.generate_slug!
  print "."
end
puts
puts

puts "Generating hosts..."
_____ = Host.create!(:name => "twitter", :url => "https://pbs.twimg.com/media/", :active => true,
              :regex => "(https?://)?pbs.twimg.com/media/(?<filename>\\w+\\.[a-z]+)")
puts "Host: #{_____.name}"
imgur = Host.create!(:name => "Imgur", :url => "https://imgur.com/", :active => true,
                     :broken_img => File.new("app/assets/images/imgur_removed.png"),
                     :regex => "(https?://)?(i\\.)?imgur.com/(?<filename>\\w+\\.[a-z]+)")
puts "Host: #{imgur.name}"
_____ = Host.create!(:name => "reddit (short)", :url => "https://i.redd.it/", :active => true,
                     :regex => "(https?://)?i.redd.it/(?<filename>\\w+\\.[a-z]+)")
puts "Host: #{_____.name}"
slimg = Host.create!(:name => "SLiMG", :url => "https://sli.mg/", :active => true,
                     :broken_img => File.new("app/assets/images/slimg_404.jpg"),
                     :regex => "(https?://)?(i\\.)?sli.mg/(?<filename>\\w+\\.[a-z]+)")
puts "Host: #{slimg.name}"
_____ = Host.create!(:name => "reddit uploads", :url => "https://reddituploads.com/", :active => false,
                     :regex => "https://(i\\.)?reddituploads.com/(?<filename>\\w+\\.[a-z]+)")
puts "Host: #{_____.name}"
_____ = Host.create!(:name => "imgflip", :url => "https://i.imgflip.com/", :active => true,
                     :regex => "(https?://)?(i\\.)?imgflip.com/(?<filename>\\w+\\.[a-z]+)")
puts "Host: #{_____.name}"
puts

untagged = <<ENDL
   001 - 0o8q729.png
   001 - ejz25rJ.jpg
   002 - CLUltpD.jpg
   003 - 0hB4QsZ.jpg
   003 - lwloca0.jpg
ENDL

tagged = {}

tagged["bernard"] = <<ENDL
       041 - JWVx6jf.png
       054 - VxZWh0d.jpg
       141 - YiBhHdO.jpg
       38 - oY0fAxd.jpg
       5H7Gkv.jpg
       5iohyR.jpg
       65 - wVzSHMo.jpg
       8e9rmd.jpg
       9CCligc.jpg
       9IH9psv.jpg
       9yn4Bm.jpg
       aHUhRgJ.jpg
       c0T3vAY.jpg
       CDFshlO.jpg
       cHsirWc.jpg
       cKBut2.png
       COgSldP.jpg
       dJjQZN.jpg
       FbJgHv.jpg
       FbmkBx.png
       fGhunq.png
       H13Gz9.jpg
       hcXtqr.jpg
       jNCwBz.jpg
       kBmRE8.jpg
       kdbCYv.jpg
       lPAPJ7.jpg
       lpLTIg.jpg
       lsJ4AO.jpg
       N9gNulA.jpg
       nZ9kME.jpg
       otD3K1.jpg
       pqfj9i.jpg
ENDL


tagged["coaxed into a snafu"] = <<ENDL
       0NRltS.png
       ut4jW6.png
       W4t8xu.png
ENDL

tagged["constantinople"] = <<ENDL
       lvfCTz8.jpg
ENDL

tagged["god emperor"] = <<ENDL
       032 - iMB707c.jpg
       rt1R6n4.jpg
       w2oMGYl.jpg
ENDL

tagged["rare donalds"] = <<ENDL
       0sy4Suu.jpg
       0Xx5JJ.jpg
       2OnYwrk.jpg
       YwBCQDIl.jpg
       ZoPKHAP.jpg
ENDL

tagged["wall"] = <<ENDL
        007 - 5k4rv8F.jpg
        021 - Lh8fVpJ.jpg
        039 - VzAbl0e.jpg
        09 - x0wwB9g.jpg
        5vawDKG.jpg
        bT4uvw.png
        FPLJIV.png
        I1hBd9h.jpg
ENDL

# \|                    =>
# \s*[+\\]---([a-z ]*)   =>  \nENDL\n\ntagged["\1"] = <<ENDL
# untagged = <<ENDL
# tagged = {}

# \.JPG                  =>  .jpg
# \.JPEG                =>  .jpeg
# \.PNG                  =>  .png

if Rails.env.development?
  puts "Generating memes..."
  failed_memes = []

  untagged = untagged.split("\n")
  untagged.each do |line|
    filename = line.split(" ")[-1]
    meme = Meme.find_meme(filename)
    if !meme
      failed_memes << filename
    end
    print "."
  end

  tagged.each do |tag_name, vals|
    vals = vals.split("\n")
    vals.each do |line|
      filename = line.split(" ")[-1]
      meme = Meme.find_meme(filename)
      if meme
        meme.tag_with(tag_name, :create => true)
      else
        failed_memes << filename
      end
      print "."
    end
  end
  puts

  if failed_memes.any?
    puts
    puts "Some memes failed:"
    failed_memes.each { |filename| puts filename }
  end
end
