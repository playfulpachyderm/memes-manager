namespace :db do
  desc "Populates tag slugs based on tag names"
  task generate_slugs: :environment do
    puts "Populating #{Tag.count} tags"
    ActiveRecord::Base.transaction do
      Tag.all.each do |tag|
        tag.generate_slug!
        print "."
      end
    end
    puts
    puts "Finished!"
  end
end
