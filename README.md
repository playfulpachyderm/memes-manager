# README

This is a memes manager.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

## To do

- feature to mark memes as duplicate
- Add meme upload option to meme creation page (instructions of how to upload to slimg)
- Tag search feature with filters
- Intelligent tagging suggestion feature
- Meme captions
- Rehosting on sli.mg
- Host management pages
- add user roles?
- Add status codes to responses with flash (401, 403, 422 etc)
- backups
- Page titles ( provide(:title => "fsjdkfsjdkfjs" )
- make whole site mobile-friendly
- make user signup work with manual approval
- salt passwords

### Non-functional todos

- Optimize seeds a bit better (don't check URL twice)
- look into the disabled tagging button (better disabled text / styling?)
- On last meme scrolling through, add dummy element to fill up the space (otherwise it becomes part of the meme which is a link)
- Improve tags index page
- Test for logging in as different user when already logged in

### Bugs

