require 'test_helper'

class TagsControllerTest < ActionDispatch::IntegrationTest
  include TagsHelper

  def setup
    @tag = tags(:murica)

    user = users(:alessio)
    log_in_as(user, "the password")
  end

  # ==== Get paths ====

  test "should get new" do
    get new_tag_path
    assert_template "tags/new"
  end

  test "should get index" do
    get tags_path
    assert_select "a[href=?]", tag_path(@tag)
    assert_select ".pagination"
  end

  test "should get show" do
    get tag_path(@tag)
    assert_template "tags/show"
    @tag.memes.each_with_index do |_, index|
      assert_select "a[href=?]", show_meme_path(@tag, index + 1)
    end
  end

  test "tag id outside valid range should 404" do
    assert_raise ActionController::RoutingError do
      get tag_path(0)
    end
    assert_raise ActionController::RoutingError do
      get tag_path(Tag.last.id + 1)
    end
  end

  test "deleted tags should 404" do
    tag = Tag.second.destroy  # ids are not in order :(
    assert_raise ActionController::RoutingError do
      get tag_path(tag)
    end
  end

  # ==== Creating new tags ====

  test "create new tag" do
    tag_name = "asdf tag"
    assert_difference "Tag.count", 1 do
      post tags_path, :params => { :tag => { :name => tag_name } }
    end
    assert Tag.find_by(:name => tag_name)
  end

  # ==== Browsing memes by tag ====

  test "browse memes by tag" do
    get show_meme_path(@tag, 2)
    assert_template "memes/show"
    assert_select ".arrow", :count => 2
    assert_select "a[href=?]", show_meme_path(@tag, 1)
    assert_select "a[href=?]", show_meme_path(@tag, 3)
  end

  test "browsing memes past endpoints should 404" do
    assert_raise ActionController::RoutingError do
      get show_meme_path(@tag, 0)
    end
    assert_raise ActionController::RoutingError do
      get show_meme_path(@tag, @tag.memes.count + 1)
    end
    assert_raise ActionController::RoutingError do
      get show_meme_path(@tag, "asdf")
    end
  end

  test "disable arrows at endpoint memes when browsing by tag" do
    get show_meme_path(@tag, 1)

    assert_select ".arrow", :count => 1
    assert_select ".arrow-left", :count => 0
    assert_select "a[href=?]", show_meme_path(@tag, 2)

    get show_meme_path(@tag, @tag.memes.count)
    assert_select ".arrow", :count => 1
    assert_select ".arrow-right", :count => 0
    assert_select "a[href=?]", show_meme_path(@tag, @tag.memes.count - 1)
  end

  test "disable arrows special case: tag with only 1 meme" do
    tag = tags(:god_emperor)
    get show_meme_path(tag, 1)
    assert_select ".arrow", :count => 0
  end

  # ==== Old URLs ====

  test "legacy tag page url using id should redirect" do
    get "/tags/#{@tag.id}"
    assert_redirected_to tag_path(@tag)
  end

  test "legacy tag page url using id should 404 if no such tag id" do
    assert_raise ActionController::RoutingError do
      get "/tags/0"
    end

    assert_raise ActionController::RoutingError do
      get "/tags/100000"
    end
  end
end
