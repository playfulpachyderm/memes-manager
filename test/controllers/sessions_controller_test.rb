require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:alessio)
  end

  test "should get new" do
    get login_path
    assert_response :success
    assert_template "sessions/new"
  end

  test "login with invalid info" do
    post login_path, :params => { :session => { :email => "",
                                                :password => "" } }
    assert_template "sessions/new"
    assert_not flash.empty?
    assert_not is_logged_in?

    # Check flash is cleared
    get root_path
    assert flash.empty?
  end

  test "non-activated users should not be able to log in" do
    milo = users(:milo)
    log_in_as(milo, "based")
    assert_not is_logged_in?
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "login as valid user, then logout" do
    log_in_as(@user, "the password")
    assert_redirected_to user_path(@user)
    follow_redirect!

    assert is_logged_in?
    assert_template "users/show"
    assert_select "a[href=?]", user_path(@user)

    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_path

    # Simulate clicking logout from a second window (check nothing breaks)
    delete logout_path

    follow_redirect!
    assert_select "a[href=?]", user_path(@user), :count => 0
  end

  test "login with remembering" do
    log_in_as(@user, "the password", :remember_me => "1")
    assert_not_nil cookies["remember_token"]
  end

  test "login without remembering" do
    log_in_as(@user, "the password", :remember_me => "0")
    assert_nil cookies["remember_token"]
  end

  test "friendly forwarding on login" do
    get edit_user_path(@user)
    log_in_as(@user, "the password")
    assert_redirected_to edit_user_path(@user)

    # ensure forwarding only happens once (i.e. forwarding url is cleared)
    assert_nil session[:forwarding_url]
  end
end
