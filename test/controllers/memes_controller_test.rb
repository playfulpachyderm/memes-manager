require 'test_helper'

class MemesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @meme = Meme.second

    admin = users(:alessio)
    log_in_as(admin, "the password")
  end

  # ==== Get paths ====

  test "should get show" do
    get meme_path(@meme)
    assert_response :success
    assert_template "memes/show"

    assert_select ".arrow", :count => 2
    assert_select ".tag", :count => 2
    assert_select "a.arrow-holder[href=?]", meme_path(@meme.id - 1)
    assert_select "a.arrow-holder[href=?]", meme_path(@meme.id + 1)

    @meme.tags.each do |tag|
      assert_select "a[href=?]", tag_path(tag)

      assert_select "form[action=?]", untag_meme_path(@meme)
      assert_select "input[name=?][value=?]", "_method", "delete"
      assert_select "input[name=?][value=?]", "tag_id", "#{tag.id}"
    end

    assert_select "form[action=?][method=?]", tag_meme_path(@meme), "post" do
      assert_select "input[type=?][name=?][value=?]", "hidden", "_method", "patch"
      assert_select "input[name=?]", "tag[name]"
    end

    assert_select "form[action=?][method=?]", meme_path(@meme), "post" do
      assert_select "input[type=?][name=?][value=?]", "hidden", "_method", "delete"
    end
  end

  test "should disable arrows for endpoint memes" do
    get meme_path(Meme.first)
    assert_select ".arrow", :count => 1
    assert_select ".arrow-left", :count => 0

    get meme_path(Meme.last)
    assert_select ".arrow", :count => 1
    assert_select ".arrow-right", :count => 0

    # check it still works if endpoints change
    Meme.first.destroy
    Meme.last.destroy

    get meme_path(Meme.first)
    assert_select ".arrow", :count => 1
    assert_select ".arrow-left", :count => 0

    get meme_path(Meme.last)
    assert_select ".arrow", :count => 1
    assert_select ".arrow-right", :count => 0
  end

  test "arrows should skip missing memes" do
    Meme.find(3).destroy

    get meme_path(Meme.find(2))
    assert_select "a.arrow-holder[href=?]", meme_path(1)
    assert_select "a.arrow-holder[href=?]", meme_path(3), :count => 0
    assert_select "a.arrow-holder[href=?]", meme_path(4)

    get meme_path(Meme.find(4))
    assert_select "a.arrow-holder[href=?]", meme_path(5)
    assert_select "a.arrow-holder[href=?]", meme_path(3), :count => 0
    assert_select "a.arrow-holder[href=?]", meme_path(2)
  end

  test "meme id outside valid range should 404" do

    assert_raise ActionController::RoutingError do
      get meme_path(0)
    end
    assert_raise ActionController::RoutingError do
      get meme_path(Meme.last.id + 1)
    end
  end

  test "deleted memes should 404" do
    Meme.find(2).destroy
    assert_raise ActionController::RoutingError do
      get meme_path(2)
    end
  end

  test "should get index" do
    get memes_path
    assert_template "memes/index"

    assert_select "li.meme_thumb", :count => Meme.count
    Meme.all.each do |meme|
      assert_select "a[href=?]", meme_path(meme)
      assert_select "img[src=?]", meme.url
    end
  end

  test "should get new" do
    get new_meme_path
    assert_template "memes/new"
  end

  # ==== Meme creation ====

  test "should create a new meme" do
    url = "https://i.imgur.com/lvfCTz8.jpg"  # crusAIDS
    assert_difference "Meme.count", 1 do
      post memes_path, :params => { :meme => { :url => url } }
    end
    assert_redirected_to meme_path(Meme.last)
  end

  test "should fail to create a new meme" do
    url = "https://imgur.com/asdfasdfasdfawefawef.jpg"
    assert_no_difference "Meme.count" do
      post memes_path, :params => { :meme => { :url => url } }
    end
    assert_template "memes/new"
    assert_select "div#error_explanation"
  end

  # ==== Meme deletion ====

  test "should destroy a meme" do
    path = "ajerklfakj"
    assert_difference "Meme.count", -1 do
      delete meme_path(@meme), :params => { :redirect_path => path }
    end
    assert_redirected_to path
    assert_not_nil flash[:success]
  end
end
