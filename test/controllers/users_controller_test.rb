require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:alessio)
    @sanders = users(:sanders)
  end

  test "how does fixtures work" do
    assert_equal @user, User.find_by(:name => "Alessio")
  end

  test "should get new" do
    get signup_path
    assert_response :success

    assert_select "form[action='/signup']"
  end

  test "should get show" do
    get user_path(@user)
    assert_response :success

    assert_select "img.gravatar"
  end

  test "should get edit" do
    log_in_as(@user, "the password")
    assert is_logged_in?
    get edit_user_path(@user)
    assert_response :success
    assert_template 'users/edit'

    assert_select "form[action='/users/#{@user.id}'][method='post']"
    assert_select "input[type='hidden'][name='_method'][value='patch']"
  end

  test "should fail to get edit if not logged in" do
    assert_not is_logged_in?

    get edit_user_path(@user)
    assert_not_nil flash[:danger]
    assert_redirected_to login_path
  end

  test "should fail to get edit if logged in as wrong user" do
    log_in_as(@sanders, "moron")
    assert is_logged_in?

    get edit_user_path(@user)
    assert_not_nil flash[:danger]
    assert_redirected_to login_path
  end

  test "should fail to update a user if not logged in" do
    assert_not is_logged_in?

    patch user_path(@user), :params => { :user => { :name => "X" + @user.name,
                                                    :email => @user.email } }
    assert_not_nil flash[:danger]
    assert_redirected_to login_path
  end

  test "should fail to update a user if logged in as wrong user" do
    log_in_as(@sanders, "moron")
    assert is_logged_in?

    patch user_path(@user), :params => { :user => { :name => "X" + @user.name,
                                                    :email => @user.email } }
    assert_not_nil flash[:danger]
    assert_redirected_to login_path
  end

  test "should get index as admin" do
    log_in_as(@user, "the password")
    get users_path
    assert_template "users/index"
    assert_select "div.pagination"
    assert_select ".users"

    User.paginate(:page => 1).each do |user|
      assert_select "a[href=?]", user_path(user), :text => user.name
      if user != @user
        assert_select "a[href=?][data-method='delete']", user_path(user),
                            :text => "delete"
      end
    end
  end

  test "get index as non-admin" do
    log_in_as(@sanders, "moron")
    get users_path
    assert_select "a[href=?][data-method='delete']", user_path(@user),
                            :text => "delete", :count => 0
  end

  test "should redirect index if not logged in" do
    get users_path
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not admin" do
    log_in_as(@sanders, "moron")
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end

  test "delete when logged in as admin should remove the user" do
    log_in_as(@user, "the password")
    assert_difference "User.count", -1 do
      delete user_path(@sanders)
    end

    assert_raise ActiveRecord::RecordNotFound do
      @sanders.reload
    end
  end
end
