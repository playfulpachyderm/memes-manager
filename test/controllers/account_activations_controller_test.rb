require 'test_helper'

class AccountActivationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @milo = users(:milo)
  end

  test "valid activation link should activate account" do
    get edit_account_activation_path("an activation token", :email => @milo.email)
    assert @milo.reload.activated?
    assert_redirected_to user_path(@milo)
    assert is_logged_in?
    assert_not flash[:success].nil?
  end

  test "invalid activation link should not work" do
    get edit_account_activation_path("gibberish", :email => @milo.email)
    assert_not @milo.reload.activated?
    assert_not is_logged_in?
    assert_not flash[:danger].nil?
  end

  test "valid activation link with wrong email should not work" do
    get edit_account_activation_path("an activation token", :email => "gibberish")
    assert_not @milo.reload.activated?
    assert_not is_logged_in?
    assert_not flash[:danger].nil?
  end
end
