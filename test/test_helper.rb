ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!
# Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new()]
class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def is_logged_in?
    begin
      return !session[:user_id].nil?
    rescue NoMethodError
      # no requests have been made yet, so no session exists
      return false
    end
  end

  def log_in_as(user)
    session[:user_id] = user.id
  end

  # Add more helper methods to be used by all tests here...
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(user, password, remember_me: '0')
    post login_path, :params => { :session => { :email => user.email,
                                                :password => password,
                                                :remember_me => remember_me } }
  end
end