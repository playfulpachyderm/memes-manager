require 'test_helper'

class HostTest < ActiveSupport::TestCase

  def setup
    @slimg = hosts(:slimg)
    @imgur = hosts(:imgur)
  end

  # ==== Scopes ====

  test "default scope should ignore inactive hosts" do
    assert_equal Host.count, Host.where(:active => true).count
    assert_equal Host.unscoped.count, Host.count + Host.unscoped.where(:active => false).count
  end

  test "slimg scope should work" do
    assert_equal Host.slimg, @slimg
    assert_equal Host.slimg.name, "SLiMG"
  end

  test "not slimg scopes should work" do
    assert_equal Host.not_slimg.length, 1
    assert_equal Host.unscoped.not_slimg.length, 2
    Host.unscoped.not_slimg.each do |host|
      assert_not_equal host.name, "SLiMG"
    end
  end

  # ==== Regex url matching ====

  test "should match slimg regex and return filename" do
    assert_equal "7QhJi3.jpg", @slimg.match("https://i.sli.mg/7QhJi3.jpg")
    assert_equal "7QhJi3.jpg", @slimg.match("https://sli.mg/7QhJi3.jpg")
  end

  test "should not match anything" do
    invalid_filename = "https://imgur.com/page?params=arg1&paramsalso=arg2"
    slimg_url = "https://sli.mg/7QhJi3.jpg"
    assert_nil @imgur.match(invalid_filename)
    assert_nil @imgur.match(slimg_url)
  end

  test "should find correct host, then return new meme" do
    meme = Host.match("https://i.sli.mg/7QhJi3.jpg")
    assert_equal @slimg, meme.host
    assert_equal "7QhJi3.jpg", meme.filename
  end

  test "should reject all hosts and return empty meme" do
    invalid_filename = "https://imgur.com/page?params=arg1&paramsalso=arg2"
    meme = Host.match(invalid_filename)
    assert_nil meme.host
    assert_nil meme.filename
  end
end
