require 'test_helper'

class TagTest < ActiveSupport::TestCase
  include Rails.application.routes.url_helpers

  def setup
    @tag = tags(:murica)
  end

  # ==== Misc ====

  test "should populate slug" do
    tag = Tag.new(:name => "Sad!!!!ONE PorcUPINES ...and stuff??")
    tag.generate_slug
    assert_equal "sad-one-porcupines-and-stuff", tag.slug
  end

  test "should populate and save slug" do
    tag = Tag.new(:name => "Sad!!!!ONE PorcUPINES ...and stuff??")
    tag.generate_slug!
    assert_equal "sad-one-porcupines-and-stuff", tag.reload.slug
  end

  test "should use slug in tag_path instead of id" do
    assert_equal "/tags/murica", tag_path(@tag)
  end

  # ==== Validation ====

  test "should be valid" do
    assert @tag.valid?
  end

  test "tags must have unique names" do
    tag2 = @tag.dup
    assert_not tag2.valid?
  end

  test "tags must have slug" do
    tag2 = Tag.new(:name => "asdfasdf")
    assert_not tag2.valid?
  end

  # ==== Memes ====

  test "find index of meme" do
    random_index = 4
    meme = @tag.memes[random_index - 1]  # should start with index 1
    assert_equal random_index, @tag.find_index_of_meme(meme)
  end

  test "should get the right meme" do
    assert_equal @tag.memes.third, @tag.meme_at(3)
    assert_equal memes(:meme5), @tag.meme_at(5)
  end

  test "meme_at should return nil if index out of range" do
    assert_nil @tag.meme_at(0)
    assert_nil @tag.meme_at(1000)
  end

  test "find next should give the next meme" do
    assert_equal 5, @tag.find_next(4, 1)
    assert_equal 3, @tag.find_next(4, -1)
  end

  test "find next should return nil if at endpoints" do
    assert_nil @tag.find_next(1, -1)
    assert_nil @tag.find_next(@tag.memes.count, 1)
    assert_nil @tag.find_next(1000, -1)
  end
end
