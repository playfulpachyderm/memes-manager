require 'test_helper'

class MemeTest < ActiveSupport::TestCase

  def setup
    @meme = memes(:meme2)
  end

  # === Misc ====

  test "verify fixtures are working" do
    assert @meme.tags.exists?(tags(:god_emperor).id)
    assert @meme.tags.exists?(tags(:murica).id)
  end

  # ==== Validations ====

  test "should be valid" do
    assert @meme.valid?
  end

  test "url should return code 200" do
    @meme.filename = "OBVIOUSLY_FAKE_FILENAME.JPG"
    assert_not @meme.valid?
    assert @meme.errors.full_messages[0].include?("404")
  end

  test "should not allow duplicate memes" do
    meme2 = @meme.dup()
    assert_not meme2.valid?
  end

  test "should allow duplicate filenames if they have different hosts" do
    meme2 = @meme.dup()
    meme2.host = hosts(:slimg)
    assert meme2.valid?
  end

  # ==== Tagging ====

  test "should add an existing tag" do
    tag = tags(:random_tag)
    assert_no_difference "Tag.count" do
      @meme.tag_with "random tag"
    end
    assert @meme.valid?
    assert @meme.tags.find(tag.id)
  end

  test "should create a new tag if passed create param" do
    tag_name = "that's bananas"
    assert_difference "Tag.count", 1 do
      @meme.tag_with tag_name, :create => true
    end
    tag = Tag.find_by(:name => tag_name)
    assert @meme.valid?
    assert @meme.tags.find(tag.id)
  end

  test "should fail to create a new tag without create param" do
    tag_name = "that's bananas"
    assert_no_difference "Tag.count" do
      assert_not @meme.tag_with tag_name
    end
  end

  test "should refuse to tag with the same tag more than once" do
    tag = @meme.tags.first
    assert_no_difference "@meme.tags.count" do
      assert_not @meme.tag_with(tag.name)
    end
  end

  test "should remove a tag" do
    assert_difference "@meme.reload.tags.count", -1 do
      assert @meme.untag(@meme.tags.first.id)
    end
  end

  # ==== Searches ====

  test "should find next memes" do
    assert_equal 1, Meme.find_next(@meme.id, -1)
    assert_equal 3, Meme.find_next(@meme.id, 1)

    assert_equal 5, Meme.find_next(memes(:meme20).id, -1)
    assert_equal 20, Meme.find_next(memes(:meme5).id, 1)
  end

  test "next meme should return nil if at endpoints or out of bounds" do
    assert_nil Meme.find_next(Meme.first.id, -1)
    assert_nil Meme.find_next(Meme.last.id, 1)
    assert_nil Meme.find_next(-133429, -1)
    assert_nil Meme.find_next(133429, 1)
  end
end
