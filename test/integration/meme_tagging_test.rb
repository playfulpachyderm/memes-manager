require 'test_helper'

class MemeTaggingTest < ActionDispatch::IntegrationTest
  include TagsHelper

  def setup
    @meme = Meme.second

    @tag = tags(:murica)
    @new_tag = tags(:random_tag)

    @referrer = show_meme_path(@tag, @tag.find_index_of_meme(@meme))
    @referrer_params = "?a=2&b=fjkf"

    admin = users(:alessio)
    log_in_as(admin, "the password")
  end

  # ==== Tagging ====

  test "should tag a meme" do
    assert_difference "@meme.tags.count", 1 do
      patch tag_meme_path(@meme), :params => { :tag => { :name => @new_tag.name } }
    end
    assert flash.empty?
    assert_redirected_to meme_path(@meme)

    follow_redirect!
    assert_select ".tag[data-id=?]", "#{@new_tag.id}"
  end

  test "should fail to tag a meme if tag doesn't exist" do
    tag_name = "OBVIOUSLY FAKE NAME rtjlrtgjks"
    assert_no_difference "@meme.tags.count" do
      patch tag_meme_path(@meme), :params => { :tag => { :name => tag_name } }
    end
    assert flash[:danger].downcase.include?("invalid")
    assert_redirected_to meme_path(@meme, :bad_tag_name => tag_name)

    follow_redirect!
    assert_select "div.field_with_errors"
    assert_select "#tag_name[value=?]", tag_name
  end

  test "should fail to tag a meme if already tagged with it" do
    tag_name = @meme.tags.first.name
    assert_no_difference "@meme.tags.count" do
      patch tag_meme_path(@meme), :params => { :tag => { :name => tag_name } }
    end
    assert flash[:danger].downcase.include?("already")
    assert_redirected_to meme_path(@meme, :bad_tag_name => tag_name)

    follow_redirect!
    assert_select "div.field_with_errors"
    assert_select "#tag_name[value=?]", tag_name
  end

  # ==== Untagging ====

  test "should untag a meme" do
    get meme_path(@meme)
    assert_select ".tag[data-id=?]", "#{@tag.id}"

    assert_difference "@meme.tags.count", -1 do
      delete untag_meme_path(@meme), :params => { :tag_id => @tag.id }
    end
    assert_redirected_to meme_path(@meme)

    follow_redirect!
    assert_select ".tag[data-id=?]", "#{@tag.id}", :count => 0
  end

  test "untagging a meme that isn't tagged should 404" do
    assert_raise ActionController::RoutingError do
      delete untag_meme_path(@meme), :params => { :tag_id => 0 }
    end
  end

  # ==== While browsing by tag ====

  test "tagging should redirect to same page when browsing by tag" do
    random_tag = tags(:random_tag)
    patch tag_meme_path(@meme), :params => { :tag => { :name => random_tag.name } },
                                    :headers => { 'referer' => @referrer + @referrer_params }
    assert_redirected_to @referrer
  end

  test "failed tagging should redirect to same page when browsing by tag" do
    name = "LOL obviously fake fsdfdfgsgrkjihyugtfv"
    patch tag_meme_path(@meme), :params => { :tag => { :name => name } },
                                    :headers => { 'referer' => @referrer + @referrer_params } # random params
    assert_redirected_to @referrer + "?" + { :bad_tag_name => name }.to_query
  end

  test "untagging should redirect to same page when browsing by tag" do
    delete untag_meme_path(@meme), :params => { :tag_id => @tag.id },
                                  :headers => { 'referer' => @referrer + @referrer_params }
    assert_redirected_to @referrer
  end
end
