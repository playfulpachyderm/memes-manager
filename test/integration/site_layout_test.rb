require 'test_helper'
include ApplicationHelper

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "page title" do
    get contact_path
    assert_select "title", full_title("Contact")
  end

  test "layout links" do
    get root_path
    assert_template "static_pages/home"
    assert_select "a[href=?]", root_path, :count => 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href*=?]", "/r/The_Donald"
  end

  test "header when not logged in" do
    get root_path

    assert_select "a[href=?]", login_path

    assert_select "a[href=?]", users_path, :count => 0
    assert_select "a[href=?]", "#",  :count => 0
    assert_select "a[href=?]", logout_path,  :count => 0

    assert_select "a[href=?]", tags_path, :count => 0

    assert_select "a[href=?]", new_meme_path, :count => 0
    assert_select "a[href=?]", new_tag_path, :count => 0
  end

  test "header when logged in as non-admin" do
    user = users(:sanders)
    log_in_as(user, "moron")
    get root_path

    assert_select "a[href=?]", login_path, :count => 0

    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", "#", :count => 2
    assert_select "a[href=?]", user_path(user)
    assert_select "a[href=?]", logout_path

    assert_select "a[href=?]", tags_path

    assert_select "a[href=?]", new_meme_path, :count => 0
    assert_select "a[href=?]", new_tag_path, :count => 0
  end

  test "header when logged in as admin" do
    user = users(:alessio)
    log_in_as(user, "the password")
    get root_path

    assert_select "a[href=?]", login_path, :count => 0

    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", "#", :count => 2
    assert_select "a[href=?]", user_path(user)
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", memes_path
    assert_select "a[href=?]", tags_path

    assert_select "a[href=?]", new_meme_path
    assert_select "a[href=?]", new_tag_path
  end
end
