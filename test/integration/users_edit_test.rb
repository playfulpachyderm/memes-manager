require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:alessio)
    @sanders = users(:sanders)
  end

  test "unsuccessful edit" do
  	log_in_as(@user, "the password")
    patch user_path(@user), :params => { :user => {
    																				:name =>  "",
                                            :email => "foo@invalid",
                                            :password =>              "foo",
                                            :password_confirmation => "bar" } }

    assert_template 'users/edit'
		assert_nil		flash[:success]
		assert_select	"#error_explanation"
  end

  test "successful edit" do
		name = "Foo Bar"
		email = "foo@bar.com"

  	log_in_as(@user, "the password")
		patch user_path(@user), :params => { :user => {
																						:name => name,
																						:email => email,
																						:password => "",
																						:password_confirmation => "" } }

		assert_not_nil  flash[:success]
		assert_select	"#error_explanation", :count => 0
		assert_redirected_to user_path(@user)

		@user.reload
		assert_equal name,		@user.name
		assert_equal email, 	@user.email
  end

  test "should not allow admin field to be changed via patch call" do
  	assert_not @sanders.admin?
  	log_in_as(@sanders, "the password")
  	patch user_path(@sanders), :params => { :user => {
  																					:admin => true,
  																					:password => "",
  																					:password_confirmation => "" } }
		 assert_not @sanders.admin?
  end
end