require 'test_helper'

class AuthorizationTest < ActionDispatch::IntegrationTest
  include TagsHelper

  def setup
    @meme = Meme.second
    @tag = tags(:murica)
    @new_tag = tags(:random_tag)
    @sanders = users(:sanders)
  end

  # ==== Meme controller ====

  test "all meme actions should be login-protected" do
    get meme_path(@meme)
    assert_redirected_to login_path

    get new_meme_path
    assert_redirected_to login_path

    get memes_path
    assert_redirected_to login_path

    assert_no_difference "Meme.count" do
      post memes_path
    end
    assert_redirected_to login_path

    assert_no_difference "@meme.tags.count" do
      delete untag_meme_path(@meme), :params => { :tag_id => @tag.id }
    end
    assert_redirected_to login_path

    assert_no_difference "@meme.tags.count" do
      patch tag_meme_path(@meme), :params => { :tag_name => @new_tag.name }
    end
    assert_redirected_to login_path

    assert_no_difference "Meme.count" do
      delete meme_path(@meme)
    end
    assert_redirected_to login_path
  end

  test "all meme actions except show and index should be admin protected" do
    log_in_as(@sanders, "moron")

    get meme_path(@meme)
    assert_response :success

    get memes_path
    assert_response :success

    get new_meme_path
    assert_redirected_to root_path

    post memes_path
    assert_redirected_to root_path

    assert_no_difference "@meme.tags.count" do
      delete untag_meme_path(@meme), :params => { :tag_id => @tag.id }
    end
    assert_redirected_to root_path

    assert_no_difference "@meme.tags.count" do
      patch tag_meme_path(@meme), :params => { :tag_name => @new_tag.name }
    end
    assert_redirected_to root_path

    assert_no_difference "Meme.count" do
      delete meme_path(@meme)
    end
    assert_redirected_to root_path
  end

  test "should get show as non-admin" do
    log_in_as(@sanders, "moron")

    get meme_path(@meme)
    assert_template "memes/show"

    assert_select "form", :count => 0
    assert_select "input", :count => 0
  end

  # ==== Tag controller ====

  test "all tag actions should be login-protected" do
    get tag_path(@tag)
    assert_redirected_to login_path

    get tags_path
    assert_redirected_to login_path

    get show_meme_path(@tag, 1)
    assert_redirected_to login_path

    get new_tag_path
    assert_redirected_to login_path

    assert_no_difference "Tag.count" do
      post tags_path
    end
    assert_redirected_to login_path
  end

  test "tag creation should be admin-protected" do
    log_in_as(@sanders, "moron")

    get tag_path(@tag)
    assert_response :success

    get tags_path
    assert_response :success

    get show_meme_path(@tag, 1)
    assert_response :success

    get new_tag_path
    assert_redirected_to root_path

    assert_no_difference "Tag.count" do
      post tags_path
    end
    assert_redirected_to root_path
  end
end
