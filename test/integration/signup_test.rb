require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup shouldn't create a user" do
    assert_no_difference "User.count" do
      post users_path, :params => { :user => { :name => "",
                                               :email => "user@invalid",
                                               :password => "foo",
                                               :password_confirmation => "bar" } }
    end

    assert_template "users/new"
    assert_select "div#error_explanation"
    assert_select "div.field_with_errors"
  end

  test "valid signup should send signup email" do
    assert_difference 'User.count', 1 do
      post users_path :params => { :user => { :name => "Alessio",
                                              :email => "alessio1@alessio.com",
                                              :password => "asdfasdf",
                                              :password_confirmation => "asdfasdf" } }
    end

    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_redirected_to root_path

    user = assigns(:user)  # access the @user variable from the controller
    assert_not user.activated?

    follow_redirect!
    assert_not flash[:info].nil?
  end
end
