require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Memes Manager"
    assert_equal full_title("Asdfwef"), "Asdfwef | Memes Manager"
  end
end