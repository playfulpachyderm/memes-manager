require 'test_helper'

class MemesHelperTest < ActionView::TestCase
  include TagsHelper

  def setup
    @tag = tags(:murica)
    @meme = memes(:meme2)
  end

  test "untag path" do
    assert_equal  "/memes/#{@meme.id}/untag", untag_meme_path(@meme)
  end

  test "tag path" do
    assert_equal "/memes/#{@meme.id}/tag", tag_meme_path(@meme)
  end

  test "next_meme_path without a tag" do
    assert_equal meme_path(@meme.id + 1), next_meme_path(nil, @meme, 1)
    assert_equal meme_path(@meme.id - 1), next_meme_path(nil, @meme, -1)
    assert_equal meme_path(memes(:meme20)), next_meme_path(nil, memes(:meme5), 1)
    assert_equal meme_path(memes(:meme5)), next_meme_path(nil, memes(:meme20), -1)
  end

  test "next_meme_path without a tag should nil if out of bounds" do
    assert_nil next_meme_path(nil, Meme.first, -1)
    assert_nil next_meme_path(nil, Meme.last, 1)
  end

  test "next_meme_path with a tag" do
    assert_equal show_meme_path(@tag, 1), next_meme_path(@tag, @meme, -1)
    assert_equal show_meme_path(@tag, 3), next_meme_path(@tag, @meme, 1)
  end

  test "next_meme_path with a tag should nil if out of bounds" do
    assert_nil next_meme_path(@tag, Meme.first, -1)
    assert_nil next_meme_path(@tag, Meme.last, 1)
  end

  test "meme_delete_redirect_path without a tag" do
    assert_equal meme_path(@meme.id + 1), meme_delete_redirect_path(nil, @meme)
  end

  test "meme_delete_redirect_path with a tag" do
    assert_equal show_meme_path(@tag, 2), meme_delete_redirect_path(@tag, @meme)
  end
end