require 'test_helper'

class TagsHelperTest < ActionView::TestCase

  def setup
    @tag = tags(:murica)
  end

  test "show meme path" do
    assert_equal show_meme_path(@tag, 2), "/tags/#{@tag.slug}/memes/2"
  end
end