require 'test_helper'

class UsersHelperTest < ActionView::TestCase
  test "gravatar_for helper" do
  	user = User.new(:name => "asdf", :email => "ajwkfe")
  	gravatar_tag = gravatar_for(user)
  	assert /<img/.match(gravatar_tag)
  	assert /src\w*=.*gravatar\.com/.match(gravatar_tag)
  end
end