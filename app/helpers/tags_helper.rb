module TagsHelper
  def show_meme_path(tag, number)
    return tag_path(tag) + "/memes/#{number}"
  end
end
