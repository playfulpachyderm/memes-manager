module ApplicationHelper
    THE_DONALD_URL = "https://www.reddit.com/r/The_Donald"
    def full_title(page_title = "")
        base_title = "Memes Manager"
        if page_title.empty?
            return base_title
        else
            return "#{page_title} | #{base_title}"
        end
    end

end
