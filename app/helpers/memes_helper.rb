module MemesHelper
  def next_meme_path(tag, meme, increment)
    if tag
      next_num = tag.find_next(tag.find_index_of_meme(meme), increment)
      return next_num ? show_meme_path(tag, next_num) : nil
    else
      next_num = Meme.find_next(meme.id, increment)
      return next_num ? meme_path(next_num) : nil
    end
  end

  def meme_delete_redirect_path(tag, meme)
    return next_meme_path(tag, meme, tag ? 0 : 1)
  end
end
