module SessionsHelper
	def log_in(user)
		session[:user_id] = user.id
	end

	def remember(user)
		user.remember
		cookies.permanent.signed[:user_id] = user.id
		cookies.permanent[:remember_token] = user.remember_token
	end

	def forget(user)
		user.forget
		cookies.delete(:user_id)
		cookies.delete(:remember_token)
	end

	def current_user
		if session[:user_id]
			@current_user ||= User.find_by(:id => session[:user_id])
		elsif cookies.signed[:user_id]
			user = User.find_by(:id => cookies.signed[:user_id])

			# check that it's the right user (matches remember token)
			if user && user.authenticated?(:remember, cookies[:remember_token])
				log_in user
				@current_user = user
			end
		end

		return @current_user
	end

	def logged_in?
		return !current_user().nil?
	end

	def log_out
		forget(current_user)
		session.delete(:user_id)
		@current_user = nil
	end

	def redirect_back_or(default)
		redirect_to(session[:forwarding_url] || default)
		session.delete(:forwarding_url)
	end

	def store_location
		session[:forwarding_url] = request.original_url if request.get?
	end

  def ensure_logged_in
    if not logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_path
    end
  end

  def ensure_admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
