class TagsController < ApplicationController
  before_action :ensure_logged_in
  before_action :ensure_admin_user, :only => [:new, :create]

  def new
    @tag = Tag.new
  end

  def show
    @tag = Tag.find_by(:slug => params[:slug]) or not_found!("tag not found")
  end

  def show_meme
    @tag = Tag.find_by(:slug => params[:slug]) or not_found!("tag not found")
    number = params[:meme_id].to_i
    @meme = @tag.meme_at(number) or not_found!("invalid meme number")

    @new_tag = Tag.new
    if params[:bad_tag_name]
      @new_tag.name = params[:bad_tag_name]
      @new_tag.errors.add(:name, "does not exist")
    end

    render 'memes/show'
  end

  def create
    @tag = Tag.new(:name => params[:tag][:name])
    @tag.generate_slug
    if @tag.save
      redirect_to tag_path(@tag)
    else
      render 'new'
    end
  end

  def update
  end

  def index
    @tags = Tag.paginate(:page => params[:page])
  end

  def destroy
  end
end
