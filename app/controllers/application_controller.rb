class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  def hello
    render html: "“¡Hola, mundo!”"
  end

  def self.not_found!(msg = "Not Found")
    raise ActionController::RoutingError.new(msg)
  end

  def not_found!(msg = "Not Found")
    ApplicationController::not_found!(msg)
  end
end
