class MemesController < ApplicationController
  before_action :ensure_logged_in
  before_action :ensure_admin_user, :except => [:show, :index]

  def new
    @meme = Meme.new(:filename => "fake filename")
  end

  def show
    @meme = Meme.find_by_id(params[:id]) or not_found!
    @new_tag = Tag.new
    if params[:bad_tag_name]
      @new_tag.name = params[:bad_tag_name]
      @new_tag.errors.add(:name, "does not exist")
    end
  end

  def create
    @meme = Host.match(params[:meme][:url])
    if @meme.save
      redirect_to meme_path(@meme)
    else
      render 'new'
    end
  end

  def untag
    @meme = Meme.find_by_id(params[:id])
    if @meme.untag(params[:tag_id])
      redirect_to base_referrer() || meme_path(@meme)
    else
      not_found!("No such tagging")
    end
  end

  def tag
    tag_name = params[:tag][:name]
    @meme = Meme.find_by_id(params[:id])
    if @meme.tag_with(tag_name)
      redirect_to base_referrer() || meme_path(@meme)
    else
      flash[:danger] = @meme.tags.exists?(:name => tag_name) ?
                    "Already tagged with that" : "Invalid tag name"
      if request.referrer
        redirect_to base_referrer({:bad_tag_name => tag_name})
      else
        redirect_to meme_path(@meme, :bad_tag_name => tag_name)
      end
    end
  end

  def update
  end

  def index
    @memes = Meme.paginate(:page => params[:page])
  end

  def destroy
    meme = Meme.find_by_id(params[:id])
    if meme
      meme.destroy
      flash[:success] = "Meme deleted"
    end
    redirect_to params[:redirect_path]
  end

  private

    def base_referrer(params_hash = nil)
      return nil if !request.referrer
      referrer = request.referrer.split("?")[0]
      if params_hash
        referrer += "?" + params_hash.to_query
      end
      return referrer
    end
end
