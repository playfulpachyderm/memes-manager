class UsersController < ApplicationController
  before_action :ensure_logged_in,  :only => [:edit, :update, :index, :destroy]
  before_action :correct_user,      :only => [:edit, :update]
  before_action :ensure_admin_user, :only => :destroy

  def new
    @user = User.new
  end

  def show
    @user = User.find_by_id(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Pls check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find_by_id(params[:id])
  end

  def update
    @user = User.find_by_id(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to user_url(@user)
    else
      render "edit"
    end
  end

  def index
    @users = User.paginate(:page => params[:page])
  end

  def destroy
    User.find_by_id(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  private
    def user_params
      return params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def correct_user
      @user = User.find_by_id(params[:id])
      if @user != current_user
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

end
