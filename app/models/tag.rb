class Tag < ApplicationRecord
  # reference:
  # #http://guides.rubyonrails.org/association_basics.html#methods-added-by-has-and-belongs-to-many
  has_and_belongs_to_many :memes

  validates :name, :presence => true, :length => { :maximum => 100 },
            :uniqueness => { :case_sensitive => false }

  validates :slug, :presence => true, :length => { :maximum => 100 },
            :uniqueness => { :case_sensitive => false }

  def rehost_if_needed
  end

  def to_param
    return slug
  end

  def generate_slug
    self.slug = name.parameterize
  end

  def generate_slug!
    generate_slug
    save!
  end

  def find_index_of_meme(meme)
    memes.where("id <= #{meme.id}").count
  end

  def meme_at(index)
    if 0 < index && index <= memes.count
      return memes.offset(index - 1).limit(1)[0]
    else
      return nil
    end
  end

  def find_next(num, increment)
    next_num = num + increment
    if next_num > 0 && next_num < memes.count
      return next_num
    else
      return nil
    end
  end
end
