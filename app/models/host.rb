class Host < ApplicationRecord
  # reference:
  # #http://guides.rubyonrails.org/association_basics.html#methods-added-by-has-and-belongs-to-many
  has_many :memes
  has_attached_file :broken_img
  validates_attachment_content_type :broken_img, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  default_scope -> { where(:active => true) }
  scope :not_slimg, -> { where("name != 'SLiMG'") }

  def self.slimg
    return Host.find_by(:name => "SLiMG")
  end

  def check_meme(filename)
    # TODO: write test for this
    resp = HTTParty.get(self.url + filename) rescue nil
    return nil if resp.nil?
    return nil if resp.code >= 400

    not_found = Paperclip.io_adapters.for(broken_img).read
    if not_found && not_found.force_encoding("UTF-8") == resp.body.force_encoding("UTF-8")
      return nil
    else
      return Meme.create(:filename => filename, :host => self)
    end
  end

  def match(meme_url)
    match_data = Regexp.new(regex).match(meme_url)
    if match_data
      return match_data[:filename]
    else
      return nil
    end
  end

  def self.match(meme_url)
    meme = Meme.new
    Host.all.each do |host|
      filename = host.match(meme_url)
      if filename
        meme.host = host
        meme.filename = filename
        break
      end
    end
    return meme
  end
end
