class Meme < ApplicationRecord
  # reference:
  # #http://guides.rubyonrails.org/association_basics.html#methods-added-by-has-and-belongs-to-many
  has_and_belongs_to_many :tags
  belongs_to :host

  default_scope -> { order(:id => :asc) }

  validate :ensure_valid_url
  validates :filename, :uniqueness => { :scope => :host }

  SLIMG_404 = File.read("app/assets/images/slimg_404.jpg")

  def tag_with(label, create: false)
    # ignore if already tagged with this tag
    return nil if tags.exists?(:name => label)

    tag = Tag.find_by(:name => label)
    if !tag
      if create
        tag = Tag.new(:name => label)
        tag.generate_slug
        tag.save
      else
        return nil
      end
    end

    self.tags << tag
    return tag
  end

  def untag(tag_id)
    tag = tags.find_by(:id => tag_id)
    if tag
      self.tags.destroy(tag)
      return tag
    else
      return nil
    end
  end

  def url
    if host
      return host.url + filename
    else
      return nil
    end
  end

  def self.find_next(num, increment)
    comparator = increment > 0 ? ">" : "<"
    order = increment > 0 ? :asc : :desc
    result = Meme.unscoped.where("id #{comparator} #{num}").order(:id => order).limit(1)
    return result.any? ? result[0].id : nil
  end

  def self.find_meme(filename)
    meme = Host.slimg.check_meme(filename)
    return meme if meme
    if !meme
      Host.not_slimg.each do |host|
        return meme if meme
        meme = host.check_meme(filename)
      end
    end
    return nil
  end

  private

    def ensure_valid_url
      begin
        code = HTTParty.get(url).code
      rescue
        errors.add(:url, "is not a proper URL")
      end

      if code != 200
        errors.add(:url, "could not be accessed properly (status: #{code})")
      end
    end
end
