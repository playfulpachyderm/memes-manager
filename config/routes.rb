Rails.application.routes.draw do
  root 'static_pages#home'

  get   '/home',    :to => 'static_pages#home'
  get   '/help',    :to => 'static_pages#help'
  get   '/about',   :to => 'static_pages#about'
  get   '/contact', :to => 'static_pages#contact'

  get       '/signup',  :to => 'users#new'
  post      '/signup',  :to => 'users#create'
  resources :users

  get     '/login',   :to => "sessions#new"
  post    '/login',   :to => "sessions#create"
  delete  '/logout',  :to => "sessions#destroy"

  resources :account_activations, :only => [:edit]

  resources :memes do
    member do
      patch   'tag'
      delete  'untag'
    end
  end

  resources :tags, :param => :slug do
    member do
      get 'memes/:meme_id', :to => "tags#show_meme"
    end
    collection do
      # support links already sent using tag IDs
      get ':id', :constraints => {  :id => /[0-9]*/ }, :to => redirect { |params|
        tag = Tag.find_by(:id => params[:id]) || ApplicationController::not_found!
        "/tags/#{tag.slug}"
      }
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
